<?php // Example 26-2: header.php
	session_start();

	require_once 'functions.php';

	if (isset($_SESSION['user'])) {
		$user = $_SESSION['user'];
		$loggedin = TRUE;
	} else {
		$user = "Guest";
		$loggedin = FALSE;
	}
	
?>