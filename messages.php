<?php // Example 26-11: messages.php
	require_once 'header.php';

	if (!$loggedin) die();

	if (isset($_GET['view'])) {
		$view = sanitizeString($_GET['view']);
	} else {
		$view = $user;
	}

	if (isset($_POST['text'])) {
		$text = sanitizeString($_POST['text']);

		if ($text != "") {
			$pm	 = substr(sanitizeString($_POST['pm']),0,1);
			add_message($user, $view, $pm, $text);
		}
	}

	if ($view == $user) {
		$name1 = $name2 = "Your";
	} else {
		$name1 = "<a href='members.php?view=$view'>$view</a>'s";
		$name2 = "$view's";
	}
	
	$profile = showProfile($view);

	if (isset($_GET['erase'])) {
		$erase = sanitizeString($_GET['erase']);
		remove_message($erase, $user);
	}
	
	$all_messages = fetch_all_messages($view);
	
	$message_list = [];
	foreach ($all_messages as $message) {
		$private = $message['pm'];
		$author = $message['auth'];
		$recipient = $message['recip'];
		$message_text = $message['message'];

		if ($private == 0 || $author == $user || $recipient == $user) {
			$date = date('M jS \'y g:ia:', $message['time']);

			if ($private == 0) {
				$content = "wrote: &quot;$message_text&quot;";
			} else {
				$content = "whispered: <span class='whisper'>&quot;$message_text&quot;</span>";
			}

			$erase = "";
			if ($recipient == $user) {
				$erase = "[<a href='messages.php?view=$view&erase=" . $message['id'] . "'>erase</a>]";
			}

			$message_list[] = "$date <a href='messages.php?view=$author'>$author</a> $content $erase<br>\r\n";
		}
	}

	if (!$num || count($message_list) < 1) {
		$message_list = "<br><span class='info'>No messages yet</span><br><br>";
	} else {
		$message_list = implode("\t\t\t", $message_list);
	}
	
	
	$main = <<<_MAIN
			<h3>$name1 Messages</h3>
			$profile
			<form method='post' action='messages.php?view=$view'>
				Type here to leave a message:<br>
				<textarea name='text' cols='40' rows='3'></textarea><br>
				Public<input type='radio' name='pm' value='0' checked='checked'>
				Private<input type='radio' name='pm' value='1'>
				<input type='submit' value='Post Message'>
			</form><br>
			$message_list
			<br><a class='button' href='messages.php?view=$view'>Refresh messages</a>
_MAIN;
	
	require 'template.php';
	
?>