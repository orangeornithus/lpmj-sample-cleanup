<?php	# mysql.php #

	//db config
	$db_host	= 'localhost';
	$db_name	= 'robinsnest';
	$db_user	= 'robinsnest';
	$db_pass	= 'rnpassword';
	
	//db connection
	$connection = new mysqli($db_host, $db_user, $db_pass, $db_name);
	if ($connection->connect_error) die($connection->connect_error);
	
	//db querier
	function query($query) {
		global $connection;
		$result = $connection->query($query);
		if (!$result) die($connection->error);
		return $result;
	}
	
	function real_result($query) {
		$result = query($query);
		if ($result->num_rows) {
			return true;
		}
		return false;
	}
	
	function fetch_all_column($query, $column_name) {
		$list = [];
		$result = query($query);
		$num = $result->num_rows;

		for ($j = 0 ; $j < $num ; ++$j) {
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$list[$j] = $row[$column_name];
		}
		
		return $list;
	}
	
	
	/* SPECIFIC QUERIES */
	//set up tables
	function create_table($name, $data) {
		query("CREATE TABLE IF NOT EXISTS $name($data)");
		return "Table `$name` was created or already exists.<br>";
	}
	
	function setup_tables($tables) {
		$result = [];
		foreach ($tables as $table_name => $table_data) {
			$result[] = create_table($table_name, implode(",", $table_data));
		}
		return implode("", $result);
	}
	
	//member management
	function is_a_member($user) {
		return real_result("SELECT * FROM members WHERE user='$user'");
	}
	
	function add_new_member($user, $pass) {
		query("INSERT INTO members VALUES('$user', '$pass')");
		die("<h4>Account created</h4>Please Log in.<br><br>");
	}
	
	function is_correct_login($user, $pass) {
		return real_result("SELECT user,pass FROM members WHERE user='$user' AND pass='$pass'");
	}
	
	function fetch_all_members() {
		$result = query("SELECT user FROM members ORDER BY user");
		return $result->fetch_all(MYSQLI_ASSOC);
	}
	
	//profile management
	function has_a_profile($user) {
		return real_result("SELECT * FROM profiles WHERE user='$user'");
	}
	
	function fetch_profile($user) {
		$result = query("SELECT * FROM profiles WHERE user='$user'");

		if ($result->num_rows) {
			$row = $result->fetch_array(MYSQLI_ASSOC);
			return stripslashes($row['text']);
		}
		
		return '';
	}
	
	function update_profile($user, $text) {
		if (has_a_profile($user)) {
			query("UPDATE profiles SET text='$text' where user='$user'");
		} else {
			query("INSERT INTO profiles VALUES('$user', '$text')");
		}
	}
	
	//friends management
	//db friend = follower
	function is_following($user, $member) {
		return real_result("SELECT * FROM friends WHERE user='$member' AND friend='$user'");
	}
	
	function is_followed_by($user, $member) {
		return real_result("SELECT * FROM friends WHERE user='$user' AND friend='$member'");
	}
	
	function add_friend($user, $member) {
		if (!is_following($user, $member)) {
			query("INSERT INTO friends VALUES ('$member', '$user')");
		}
	}
	
	function remove_friend($user, $friend) {
		query("DELETE FROM friends WHERE user='$friend' AND friend='$user'");
	}
	
	function fetch_user_following($view) {
		return fetch_all_column("SELECT * FROM friends WHERE friend='$view'", 'user');
		
	}
	
	function fetch_following_user($view) {
		return fetch_all_column("SELECT * FROM friends WHERE user='$view'", 'friend');
	}
	
	//messages management
	function add_message($author, $recipient, $privacy, $text) {
		$time = time();
		query("INSERT INTO messages VALUES(NULL, '$author', '$recipient', '$privacy', $time, '$text')");
	}
	
	function remove_message($id, $recipient) {
		query("DELETE FROM messages WHERE id=$id AND recip='$recipient'");
	}
	
	function fetch_all_messages($recipient) {
		$result = query("SELECT * FROM messages WHERE recip='$recipient' ORDER BY time DESC");
		return $result->fetch_all(MYSQLI_ASSOC);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

?>