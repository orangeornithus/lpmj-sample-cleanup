# This repo 
uses the robinsnest sample from [Learning PHP, MySQL, and Javascript 4th Edition](http://lpmj.net/4thedition/) 
and the power of versioning to showcase transformation to good practices such as separation of concerns. 

## Log (reverse chronological)
Updated template to use a class format; implemented template across pages.
	* Added message functions to mysql.php; implemented functions in messages.php; tested; updated readme log.
	* Added friend list functions to mysql.php; implemented functions in friends.php; added inferred missing page title; tested.
	* Added member list function to mysql.php; implemented function in members.php with additional cleanup; tested.
Continued portability of queries.
Separated concerns of showProfile function across several pages.
	* Added profile functions to mysql.php; implemented functions in profile.php; tested.
	* Added member login function to mysql.php; implemented function in login.php; tested.
	* Added member signup functions to mysql.php; implemented functions in checkuser.php and signup.php; tested.
	* Changed setup.php to use an array of the tables for readability; separated concerns; tested.
	* Moved query-related functions and config from functions.php to mysql.php; added require mysql.php.
	* Created page mysql.php; moved mysql-related data from functions.php into page; refined queries.
Began portability of queries.
Styles.css superficial cleanup.
Changed header nav to use arrays; updated readme log.
	* checkuser.php, functions.php superficial cleanup.
	* messages.php separated with superficial cleanup; tested.
	* friends.php separated with cleanup; tested.
	* members.php mostly separated with superficial cleanup; tested.
	* - identified and fixed bug in header.php where header would not show properly in XP browsers without the <base> added.
	* profile.php separated with superficial cleanup; tested; User2 profile added.
	* logout.php separated with superficial cleanup; tested.
	* login.php separated with superficial cleanup; tested.
	* signup.php separated with superficial cleanup; tested; User6 created.
	* index.php separated with superficial cleanup; tested.
	* header.php separated with superficial cleanup; tested.
Began separation of concerns (using native PHP templating styles.)
Javascript.js superficial cleanup.
Changed signup password field type.
Moved signup script to separate page.
Changed 2-space indentation to more accessible tabbed indentation.
All four friend statuses now available on User1.
Login as User5; followed User1.
Login as User2; followed back User1 and sent public and private messages to User1.
Login as User1; set up profile, avatar, and followed 2 other members; logout.
Set up five test users; security bug noted (plain text passwords on signup.)
Dev environment now loads successfully.
Ran setup.php.
Created db user and db.