<?php // Example 26-7: login.php
	require_once 'header.php';
	
	$error = $user = $pass = "";

	if (isset($_POST['user'])) {
		$user = sanitizeString($_POST['user']);
		$pass = sanitizeString($_POST['pass']);
		
		if ($user == "" || $pass == "") {
				$error = "Not all fields were entered<br>";
		} else {
			if (!is_correct_login($user, $pass)) {
				$error = "<span class='error'>Username/Password invalid</span><br><br>";
			} else {
				$_SESSION['user'] = $user;
				$_SESSION['pass'] = $pass;
				$main = "You are now logged in. Please <a href='members.php?view=$user'>click here</a> to continue.<br><br>";
				require 'template.php';
				die();
			}
		}
	}

	$main = <<<_MAIN
			<h3>Please enter your details to log in</h3>
			<form method='post' action='login.php'>$error
				<span class='fieldname'>Username</span><input type='text' maxlength='16' name='user' value='$user'><br>
				<span class='fieldname'>Password</span><input type='password' maxlength='16' name='pass' value='$pass'><br>
				<span class='fieldname'>&nbsp;</span>
				<input type='submit' value='Login'>
			</form>
_MAIN;
	
	require 'template.php';
	
?>