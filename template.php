<?php	
	
	/*
		Don't knock - this is native php templating at its finest!
		It's actually, genuinely quite good. 
		I'm happier with this somewhat hacky looking setup than 
		ANY template system I've tried so far
		because it's PERFECT in the editor and FLAWLESS to execute.
		All I have to do is include this page.
		When things go wrong, debugging is as simple as any quality template engine.
	*/
	class Template {
		public $title;
		public $base;
		public $nav_error;
		private $nav_list;
		public $nav_links;
		public $main_content;
		private $scripts = [];
		public $script_list;
		
		function choose_nav_list($loggedin, $user) {
			if ($loggedin) {
				$this->nav_error = '';
				$this->nav_list = [
					"Home" => "members.php?view=$user",
					"Members" => "members.php",
					"Friends" => "friends.php",
					"Messages" => "messages.php",
					"Edit Profile" => "profile.php",
					"Log out" => "logout.php"
				];
			} else {
				$this->nav_error = "<span class='info'>&#8658; You must be logged in to view this page.</span><br><br>";
				$this->nav_list = [
					"Home" => "index.php",
					"Sign up" => "signup.php",
					"Log in" => "login.php"
				];
			}
		}
		
		function create_nav_links() {
			$nav = [];
			foreach ($this->nav_list as $nav_name => $nav_url) {
				$nav[] = "<li><a href='$nav_url'>$nav_name</a></li>\r\n";
			}
			$this->nav_links = implode("\t\t\t", $nav);
		}
		
		function add_script($script_location) {
			$this->scripts[] = "<script src='$script_location'></script>";
		}
		
		function create_script_list() {
			$this->script_list = implode(PHP_EOL . "\t\t\t", $this->scripts);
		}
	}
	
	$page = new Template;
	$page->title = "$appname ($user)";
	$page->base = "https://localhost/lpmj-sample-cleanup/";
	
	$page->choose_nav_list($loggedin, $user);
	$page->create_nav_links();
	
	$page->create_script_list();
	
	$page->main_content = $main;
	
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?=$page->title?></title>
		<base href='<?=$page->base?>'>
		<link rel='stylesheet' href='styles.css' type='text/css'>
	</head>
	<body>
		<center><canvas id='logo' width='624' height='96'><?=$appname?></canvas></center>
		<div class='appname'><?=$page->title?></div>
		<script src='javascript.js'></script><br>
		<ul class='menu'>
			<?=$page->nav_links?>
		</ul><br>
		<?=$page->nav_error?>
		<div class='main'><?=$page->main_content?></div><br>
		<?=$page->script_list?>
	</body>
</html>