<?php // Example 26-10: friends.php
	require_once 'header.php';

	if (!$loggedin) die();

	if (isset($_GET['view'])) {
		$view = sanitizeString($_GET['view']);
	} else {
		$view = $user;
	}

	if ($view == $user) {
		$name1 = $name2 = "Your";
		$name3 = "You are";
	} else {
		$name1 = "<a href='members.php?view=$view'>$view</a>'s";
		$name2 = "$view's";
		$name3 = "$view is";
	}
	
	$profile = showProfile($view);

	$followers = $following = [];
	
	$followers = fetch_following_user($view);
	$following = fetch_user_following($view);

	$mutual	= array_intersect($followers, $following);
	$followers = array_diff($followers, $mutual);
	$following = array_diff($following, $mutual);
	
	
	$friend_lists = [];
	$friend_lists[] = create_friend_list("$name2 mutual friends", $mutual);
	$friend_lists[] = create_friend_list("$name2 followers", $followers);
	$friend_lists[] = create_friend_list("$name3 following", $following);
	
	$has_friends = count(array_filter($friend_lists, "strlen"));

	if (!$has_friends) {
		$friend_lists = "<br>You don't have any friends yet.<br><br>";
	} else {
		$friend_lists = implode("\t\t\t", $friend_lists);
	}
	
	function create_friend_list($title, $friends) {
		if (sizeof($friends)) {
			$friend_list = [];
			foreach ($friends as $friend) {
				$friend_list[] = "<li><a href='members.php?view=$friend'>$friend</a></li>\r\n";
			}
			$friend_list = implode("\t\t\t\t", $friend_list);
			
			return "<span class='subhead'>$title</span><ul>\r\n$friend_list</ul>\r\n";
		}
		
		return "";
	}

	
	$main = <<<_MAIN
			<h3>$name1 Friends</h3>
			$profile
			$friend_lists
			<a class='button' href='messages.php?view=$view'>View $name2 messages</a>
_MAIN;
	
	require 'template.php';
	
?>
