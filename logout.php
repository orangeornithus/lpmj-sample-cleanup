<?php // Example 26-12: logout.php
	require_once 'header.php';

	if (isset($_SESSION['user'])) {
		destroySession();
		$logout_message = "You have been logged out. Please <a href='index.php'>click here</a> to refresh the screen.";
	} else {
		$logout_message = "<br>You cannot log out because you are not logged in";
	}
	
	$main = "$logout_message<br>";
	require 'template.php';
?>