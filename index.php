<?php // Example 26-4: index.php
	require_once 'header.php';
	
	if ($loggedin) {
		$login_message = "$user, you are logged in";
	} else {
		$login_message = 'please sign up and/or log in to join in';
	}
	
	$main = "<br>Welcome to $appname, $login_message";
	require 'template.php';
	
?>