<?php // Example 26-1: functions.php

	require_once 'mysql.php';
	
	$appname = "Robin's Nest"; // ...and preference

	function destroySession() {
		$_SESSION=array();

		if (session_id() != "" || isset($_COOKIE[session_name()])) {
			setcookie(session_name(), '', time()-2592000, '/');
		}

		session_destroy();
	}

	function sanitizeString($var) {
		global $connection;
		$var = strip_tags($var);
		$var = htmlentities($var);
		$var = stripslashes($var);
		return $connection->real_escape_string($var);
	}

	function showProfile($user) {
		$profile = "<section id='profile'>";
		
		if (file_exists("$user.jpg")) {
			$profile .= "<img src='$user.jpg' style='float:left;'>";
		}
		
		$profile .= fetch_profile($user) . "<br style='clear:left;'><br></section>\r\n";

		return $profile;
	}
	
?>
