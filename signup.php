<?php // Example 26-5: signup.php
	require_once 'header.php';

	
	
	
	$error = $user = $pass = "";
	if (isset($_SESSION['user'])) destroySession();

	if (isset($_POST['user'])) {
		$user = sanitizeString($_POST['user']);
		$pass = sanitizeString($_POST['pass']);

		if ($user == "" || $pass == "") {
			$error = "Not all fields were entered<br><br>";
		} else {
			if (is_a_member($user)) {
				$error = "That username already exists<br><br>";
			} else {
				$main = add_new_member($user, $pass);
				require 'template.php';
				die();
			}
		}
	}
	
	
	$main = <<<_MAIN
			<h3>Please enter your details to sign up</h3>
			<form method='post' action='signup.php'>$error
				<span class='fieldname'>Username</span>
				<input type='text' maxlength='16' name='user' value='$user' onBlur='checkUser(this)'><span id='info'></span><br>
				<span class='fieldname'>Password</span>
				<input type='password' maxlength='16' name='pass' value='$pass'><br>
				<span class='fieldname'>&nbsp;</span>
				<input type='submit' value='Sign up'>
			</form>
_MAIN;
	
	
		$script = "\n\r<script src='checkuser.js'></script>\n\r";
	
	
	require 'template.php';
?>