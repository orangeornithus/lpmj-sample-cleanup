<?php // Example 26-9: members.php
	require_once 'header.php';

	if (!$loggedin) die();

	//!\\ this section should be in a different page
	if (isset($_GET['view'])) {
		$view = sanitizeString($_GET['view']);
		
		if ($view == $user) {
			$name = "Your";
		} else {
			$name = "$view's";
		}
		
		$profile = showProfile($view);
		$main = <<<_MAIN
			<h3>$name Profile</h3>
			$profile
			<a class='button' href='messages.php?view=$view'>View $name messages</a><br>
_MAIN;

		require 'template.php';
		die();
	}
	//!\\

	if (isset($_GET['add'])) {
		$add = sanitizeString($_GET['add']);

		add_friend($user, $add);
	} elseif (isset($_GET['remove'])) {
		$remove = sanitizeString($_GET['remove']);
		remove_friend($user, $remove);
	}

	$all_members = fetch_all_members();
	$num = count($all_members);
	
	$member_list = [];

	for ($j = 0 ; $j < $num ; ++$j) {
		$member = $all_members[$j]['user'];
		
		if ($member == $user) continue;

		$user_follows = is_following($user, $member);
		$follows_user = is_followed_by($user, $member);
		
		$follow_option = "drop";
		
		if ($user_follows && $follows_user) {
			$friendship_status = "&harr; is a mutual friend";
		} elseif ($user_follows) {
			$friendship_status = "&larr; you are following";
		} elseif ($follows_user) {
			$friendship_status = "&rarr; is following you";
			$follow_option = "recip"; 
		} else {
			$friendship_status = "";
			$follow_option = "follow";
		}
		
		if (!$user_follows) {
			$action = "add";
		} else {
			$action = "remove";
		}
		
		$member_profile_link = "<a href='members.php?view=$member'>$member</a>";
		$member_action_link = "[<a href='members.php?$action=$member'>$follow_option</a>]";
		
		$member_list[] = "<li>$member_profile_link $friendship_status $member_action_link</li>\r\n";
	}
	
	$member_list = implode("\t\t\t\t", $member_list);

	
	$main = <<<_MAIN
			<h3>Other Members</h3>
			<ul>
				$member_list
			</ul>
_MAIN;
	
	require 'template.php';
	
?>
